# Michael 2020 Developer Evangelism Plan


1. [Purpose](#purpose)
1. [Focus Areas](#focus-areas)
    - 2.1. [Team: Ops in DevOps](#team-ops-in-devops)
    - 2.2. [Team: Audience Focus](#team-audience-focus)
    - 2.3. [Team Goals](#team-goals)
    - 2.4. [My Goal](#my-goal)
1. [Thought Leadership](#thought-leadership)
    - 3.1. [How to Advance Thought Leadership](#how-to-advance-thought-leadership)
    - 3.2. [Avenues for Thought Leadership](#avenues-for-thought-leadership)
    - 3.3. [External Avenues for Content](#external-avenues-for-content)
1. [Events](#events)
    - 4.1. [Regional Events](#regional-events)
1. [Metrics](#metrics)
1. [Reach new Audiences](#reach-new-audiences)
    - 6.1. [Push the Web IDE](#push-the-web-ide)
    - 6.2. [Include the Admins](#include-the-admins)
    - 6.3. [Packages for everyone](#packages-for-everyone)
    - 6.4. [Share our experience with dogfooding](#share-our-experience-with-dogfooding)
1. [Ideas](#ideas)
    - 7.1. [Blogs](#blogs)
    - 7.2. [Talks](#talks)
    - 7.3. [Webcasts and Trainings](#webcast-and-trainings)
    - 7.4. [Demos](#demos)
    - 7.5. [Social](#social)
    - 7.6. [Product Feedback Loop](#product-feedback-loop)
    - 7.7. [Contribution areas](#contribution-areas)
    - 7.8. [Research areas](#research-areas)


<a id="purpose"></a>
## Purpose

This document provides insights into my Technical Evangelism plans for 2020. It covers my story telling and thought leadership evolvement as Developer Evangelist on our team.

Not everything written here is going to happen, and thoughts and ideas may change over time. When linking or referring to here, please keep this in mind
and rather ask via Slack or schedule a short meeting to clear things up. In case you want to follow along with specific topics and need our TE resources, please follow our [request template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/technical-evangelist-request.md) and create a new issue in the [Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues).

<a id="focus-areas"></a>
## Focus Areas

### Team: Ops in DevOps

With my background of leading an open source monitoring project for ~11 years in both, backend development and community support, I have a strong knowledge about challenges and solutions the monitoring world. With the shifted mindset moving from classic service monitoring to a cloud native observability world, we need to raise awareness and propose best practices. A container cluster in Kubernetes is able to heal itself, and poll-based monitoring tools cannot determine its state reliably. I'm also coming from the administration and ops side with having managed large infrastructures in student dorms, at the University of Vienna/ACO.net and at NETWAYS. The pain without automation (Puppet, Ansible) and doing manual work is something I praise to avoid.

I'm active in the Dev(Sec)Ops community with proposing workflows, learned lessons and anticipate in culture discussions at [DevOpsDays](https://www.netways.de/en/blog/2019/10/31/devopsdays-ghent-celebrating-10-years-devops-culture/). Additional venues came from this community, with Config Management Camp and DeliveryConf where I can push my thought leadership.

CI/CD matches both, Dev and Ops. With my background of backend development (C/C++, Golang) I'm able to understand typical developer workflows and problems, shifting the mindset to operations where the software is actually run and battle-tested. Release management covers the project management part with the last pipeline in deploying to production. Our mission is to enable people to automate this workflow and do not halt before the production job is run.

I also want to become the "Sec" in DevSecOps and be the chaos monkey making the on-call scenarios more easy. If not covered in full, I would take steps to prepare the topics and area for future hires.

### Team: Audience Focus

- Open source and online communities
- Social media followers and hashtags: #devops, #devsecops, #observability, #monitoringlove, #cfgmgmtcamp
- SIG members (CNCF, CDF)
- Competitors, pushing towards discussion and thought exchange
- Business leaders seeing our actions

### Team Goals

- Combine the Dev, Sec, Ops and Cloud native areas and become experts all together.
- Foster a great relationship with Open source communities.
- Take the lead and become a role model for users, heroes and community evangelist.
- Enable the GitLab team to efficiently access our resources and knowledge.
- Share our background and experiences in our team and push each other. Everyone has unique items in their chest.
- Support Brendan in CI/CD with developer specific variations (C++, etc.)
- Be open for collaborations and chats
- TE Collaboration or Think Big sessions in Zoom for everyone
    - We should live-stream to YT GitLab Unfiltered

### My Goal

Become a thought leader in these areas:

- Observability/Monitoring with logs, events, tracing, states, alerts, notifications, reporting.
- Automation and Config Management, enabling easier deployments and highlighting community work.
- Security for CI/CD and Zero Trust

Support the GitLab team with:

- Blog posts and webcasts in the above topics
- Local content review/generation in German (DACH area)

Support the wider community with feedback and help on building a trustful environment, and help with observability best practices.

Create a feedback loop with product managers:

- Ops+CI, Monitor: Kevin Chu
- Ops+CI, Health: Sarah Waldner
- Ops+CI, Package: Tim Rizzi
- Ops+CI, Release: Jackie Meshell
- Ops+CI, Templates: Thao Yeager

## Thought Leadership

### How to Advance Thought Leadership

- Become a contributor to the Prometheus project and join Ben
    - Help maintain Node-Exporters, add packages (RPM/DEB), help with build tooling, documentation
    - Community help and social
    - Working issue: https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/2014
- Put a strong focus on Tracing and Chaos Engineering as topics for 2020
- Select talk topics which
    - Aim to solve a long grown problem
    - Tell a story with different sides to discuss afterwards
- Share webcasts, demos and snippets/code which help people solve their problem or get a free training.
- Pick new projects, try them out, and help write good documentation. Use that gained knowledge to generate blog content.
- Become a community tech leader with helping users. People's trust is earned with helping them and later engage.
    - [Prometheus Community](https://prometheus.io/community/)
    - [Grafana](https://community.grafana.com/)
    - [GitLab Community](https://forum.gitlab.com)
    - [Elastic](https://discuss.elastic.co/), [Graylog](https://community.graylog.org/)
- Join communities like https://codingcoach.io/ and invest into mentoring others.
- Follow [#mentoringmonday](https://twitter.com/hashtag/mentoringmonday?f=live) and offer a special Calendly picker.
- I'm the domain owner of monitoring-portal.org - maybe re-invent this with GitLab pages and opinionated discussions.
- Learn from GitLab ops team and propose best practices together
    - Create Grafana dashboards, push towards community feedback, etc. - make it easier for admins.
- Share ideas wit the Ops monitor/health team

### Avenues for Thought Leadership

- Conferences and events in the Ops area
- SIGs, e.g. [Security](https://github.com/cdfoundation/sig-security)
- Live-tweet from events with specific hash tags, push strong opinions in tweets, not just slide summaries.
- Open source community forums
- Developer mailing lists and channels
- GitHub issues (and discussions when implemented (beta))
- YouTube/Twitch webcasts/podcasts
- Newsletters (quality content and tweets)
- Meetups and local conferences (from smaller to bigger, sharing a message is sometimes easier in smaller groups)
- Local content in German, published media articles and blog posts

### External Avenues for Content

- [My blog](https://dnsmichi.at) (generate reach via Twitter/LinkedIn shares)
- [Dev.to](https://dev.to/dnsmichi)
- [Medium](https://medium.com/@dnsmichi)
- [Opensource.com](https://opensource.com)
- Newsletters: [Monitoring.weekly](https://monitoring.love/), [cron.weekly](https://ma.ttias.be/cronweekly/), [devopsish](https://devopsish.com/categories/weekly/)
- [TheNewStack](https://thenewstack.io/) via partnership.
- [PuppetForge](https://forge.puppet.com/). Reach out to [David Schmitt](https://twitter.com/dev_el_ops).
- [Voxpupuli](https://voxpupuli.org/) being the voice of the Puppet community. Reach out to [Tim Meusel](https://twitter.com/bastelsblog).
- [InfluxDB](https://www.influxdata.com/). Reach out to [David Mc Kay](https://twitter.com/rawkode).
- [Sensu](https://sensu.io/). Reach out to [Sean Porter](https://twitter.com/portertech) and [Caleb Hailey](https://twitter.com/calebhailey).
- [Graylog](https://www.graylog.org/). Reach out to [Lennart Koopmann](https://twitter.com/_lennart).
- [Elastic](https://www.elastic.co/). Reach out to [Philipp Krenn](https://twitter.com/xeraa).


## Events

Check our [TE event sheet](https://docs.google.com/spreadsheets/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit?ts=5e61878d#gid=0) first.

- [O'Reilly Infrastructure & Ops](https://conferences.oreilly.com/infrastructure-ops)
- [Monitorama](https://monitorama.com)
- [FOSDEM](https://fosdem.org). CfgMgmt, CI/CD, Observability dev rooms.
- [DevOpsWorld](https://www.cloudbees.com/devops-world)

Suggested Events:

- [DeliveryConf](https://www.deliveryconf.com/). For EU in 2021, I'm in the orga team.
- [PromCon](https://promcon.io). NA and EU.
- [DevOpsCon](https://devopscon.io/). CfP only.

### Regional Events

Suggestions for review.

#### Conferences

- [ContainerDays EU](https://www.containerdays.io/)
- [CfgMgmtCamp](https://cfgmgmtcamp.eu/)
- [Open Source Automation Days](https://osad-munich.org/)
- [Continuous Lifecycle](https://www.continuouslifecycle.de/) presented by Heise/iX. CfP only.
- [ContainerConf](https://www.containerconf.de/) presented by Heise/iX. CfP only.
- [OSMC](https://osmc.de). CfP only.
- DevOpsDays in Europe

#### Meetups

Meetups are events that I plan to attend on a regular basis, if traveling makes sense. Everything which is located in the Nuremberg/Munich area can be reached within 2 hours range. Berlin/Vienna is 4h+ away, would need accommodation. Friends are located in the bigger cities in Austria/Germany.

##### Nuremberg

- [DevOps Camp Nuremberg](https://devops-camp.de/). Estimated audience reach: 200.
- [Kubernetes Nuremberg (CNCF Group)](https://www.meetup.com/Kubernetes-Nurnberg/)
- [DevOps Nuremberg](https://www.meetup.com/devops-nuernberg/)
- [Hackerkegeln by DATEV](https://hackerkegeln.de/)

##### Germany

- [GitLab Meetup Berlin](https://www.meetup.com/berlin-gitlab-meetup/)
- [Prometheus Berlin](https://www.meetup.com/Berlin-Prometheus-Meetup/)
- [GitLab Meetup Hamburg](https://www.meetup.com/GitLab-Meetup-Hamburg/)
- [Icinga Users Berlin](https://www.meetup.com/Icinga-Users/)

##### Austria

- [DevOps & Security Vienna](https://www.meetup.com/Vienna-DevOps-Security/)
- [Icinga Users Austria](https://www.meetup.com/Icinga-Users-Austria/)

##### Europe

- [Cloud Native Prague](https://www.meetup.com/Cloud-Native-Prague/)
- [GitLab Meetup London](https://www.meetup.com/London-Gitlab-Meetup-Group/)
- [Linuxing in London](https://www.meetup.com/Linuxing-In-London/)
- [Metrics and Monitoring London](https://www.meetup.com/London-Metrics-and-Monitoring/)



## Metrics

- Participation in TE requested help.
    - Raised demand for TE support, measured from request rate.
    - Blog posts published, with the main focus on quality not quantity.
- Twitter analytics. Tweet audience reach, interactions, likes, retweets. Toy and test things with different phrasing and topics, report these metrics.
- External guest blog posts
- Visibility in encouraging and pushing users
- Audience reach and activity in community channels (e.g. impact on GitLab Discourse)
- Highlights in newsletters
- Back reference links to workshops and trainings
- Mentions/participation in product feedback issues (needs organization/tracking, may be automated with actions)
    - Kindly ask product managers to add their feedback
    - Consider having a release retrospective item with how much TE input they had


## Reach new Audiences

GitLab is well known as Git repository management tool with added project management capabilities.

Specifically extract CE and EE features, and evaluate a possible audience and community where we can start to contribute.

- Security in pipelines - which business and open source areas are affected by this?
- Dashboards for Observability - is this something which could replace the Grafana requirement and help administrators focus on their tool stack?
- Features where we know that the documentation needs improvements (learned from forum support) and we can work together to improve it, and to raise awareness who actually uses features.
- Evaluate integrations between tools, and identify those with bottlenecks, not maintained, non-existing. Propose and discuss maintainership, e.g. "GitLab CI with GitHub Actions".
- Evaluate big players like OpenShift. Generate content and talk topics out of their aspects. E.g. Openshift replaces Jenkins with Tekton, and Docker with Podman.
- Automation, enable users to quick start their GitLab experience
    - Contribute to Puppet/Ansible/Chef/Salt modules to enable full CI/CD usage
    - https://forge.puppet.com/puppet/gitlab
    - https://docs.ansible.com/ansible/latest/search.html?q=gitlab&check_keywords=yes&area=default

Push the ideas below regularly on Twitter. Add polls, ask the community and include the product manager's twitter handles. Example: [Web IDE Feature Requests](https://twitter.com/dnsmichi/status/1236602975365345281)

### Push the Web IDE

Collect ideas and issues to improve the Web IDE experience. Provide blog posts, web casts and screenshots on how to leverage this with CI feedback and monitoring.

- Demo videos, clips showing how developers can profit
- Compare against other IDEs
- Explain the workflows with CI/CD
- Inspect running containers (CI, k8s, cloud) from the Web IDE. Inspired by VS Code extension: https://code.visualstudio.com/blogs/2019/10/31/inspecting-containers
- Evaluate [GitPod](https://www.gitpod.io/features/) and open according feature requests for the Web IDE

### Include the Admins

- Enable them with self-monitoring and health status
- Allow them to understand which CI components their developers need.
    - Ensure that CI is easy to try
    - Use the simple exercises from training slides
- Enable them to use Docker and build images.
    - Evaluate how others build images, e.g. https://getintodevops.com/blog/the-simple-way-to-run-docker-in-docker-for-ci
    - Evaluate how to provide multi version language image templates (in one Docker image or many) and Matrix builds

### Packages for everyone

Collect feedback on what open source developers need, the easy-to-install experience with packages. Keep best practices for different language, as C++ requires compiling whereas Node.js and Ruby provoke a long list of dependency management.

Build everything together and aim to integrate/replace Artifactory.

- Integrates the CI builds e.g. with Jenkins. GitLab is not supported yet.
- https://www.jfrog.com/confluence/display/RTF/Build+Integration
- https://www.jfrog.com/confluence/display/RTF/Jenkins+Artifactory+Plug-in
- https://github.com/jfrog/jenkins-artifactory-plugin
- Relevant for learning and pushing the product: https://about.gitlab.com/direction/package/package_registry/
- Example guide from GitLab: https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/index.html

### Share our experience with dogfooding

We also will be using GitLab.com differently to others.

- TE Organization, Issues, Epics, Issue Boards.
- Content and code snippets, re-usable libraries and clients for production.
- Demo playgrounds
- Manage TODO lists
- Extract metrics from our wide-spread activities

From these experiences, evaluate whether others could benefit from this as well. Propose new product ideas to the team. One item are for example replacing Microsoft To-Do with a GitLab app (discussed that with Brendan on Twitter a while ago).

- Allow to add notes, labels, organize them. Replace Microsoft To-Do/Wunderlist, Evernote, etc. with reminders also showing up
- Maybe explore a whole new feature area, outside of GitLab issues and project management


## Ideas

Vision: Should help increase the audience reach and benefit the GitLab team.

Many tools and technologies I maintain in my stack are self-taught from attending conferences, watching talks and trying them out on my own. Whenever there is a Vagrant/Docker integration available, I keep using this. If not, I create my own demo sandbox. Mostly with the idea in mind that others can find this useful as well.

> Pushing opinions on bleeding edge tools further can be helped with actual visible appealing demos, or graphics. Or just documentation how to get users started by themselves.

### Blogs

- **Prometheus for Beginners** Share my first steps with Prometheus with advancing to website monitoring, services, etc.
- **Prometheus in a distributed view** Advanced techniques I need to learn, Thanos and more.
- **From Monitoring to Observability**. Pick specific monitoring tasks and workflows, compare classic service monitoring with cloud native metric focused challenges. Add best practices e.g. on how to monitor an application cluster.
- **Build packages for better installation experience**. RPM/DEB packaging is hard to learn, a blog series and repositories will help. This will create unique content and help shape our vision with GitLab package registries.
- **Puppet**. Share insights on Puppet modules relevant to us. Work with David Schmitt on the PuppetForge experience with howtos and getting started blogs.
- **Ansible in the wild**. Galaxy is hard to use, encourage people for good playbooks. Combine this with Terraform partnerships.
- **DNS**. Include old and new things, go along with "Everything is a DNS problem". DoH (DNS over HTTPS is a sensitive topic for example)
- **Git Best Practices from a Developer's workflow**: Add short tips&tricks from my previous Git/GitLab trainings.
- **The latest in C++/Python/Ruby/...**. Do research on the language, try things and share the latest findings. This is a perfect spot for including CI/CD and unit tests.
- **Life of a Developer Evangelist** [Share](https://twitter.com/dnsmichi/status/1235210840434978818) what it is like and what we do on a daily/weekly basis to encourage others to do the same. This includes **everything** from my tech-evangelism docs, but leveraged into smaller pieces as with creating this document :)
- **Here is a trap, thank you** Tackle the problem with SNMP traps and event processing pipelines. Follow along with telemetry streaming for network hardware.
- **Incident management based on events/alerts** A common use case for monitoring integration is with ticket systems. `Alert->Ticket, Acknowledge the problem, Recovery->Close Ticket` workflows. Tackle these challenges with current tools and compare best practices. Build a knowledge base for future talks.
- **IoT Event processing with GitLab's observability stack.** Working title. Should cover the idea of IoT and smarthome devices which send e.g. their temperature. Why not find a use case how GitLab cna process that data, create fancy dashboards and update tickets or commit measurements. I wrote an [iX article](https://www.heise.de/select/ix/2018/4/1522451271629282) on IoT monitoring in the past.
- **My website is so slow. Have you heard about Tracing?**
    - Also, look into CNCF projects and verify whether they need support for OpenTracing/Jaeger/OpenTelemtry. Create PRs, and blog about these additions.
    - Example for [C++](https://www.netways.de/blog/2020/01/09/from-monitoring-to-observability-distributed-tracing-with-jaeger/)


#### GitLab Feature Focused

- **Release Management Series**. Follow up on the [Release Evidence Whitepaper](https://gitlab.slack.com/archives/C1BBL1V3K/p1583413901103900), and add general ideas and best practices how release management happens. Steps to prepare, best practices with a RELEASE.md, etc.
    - [Workflows](https://forum.gitlab.com/t/gitlab-ci-cd-pipeline/32773/2), Tests, Documentation, [good Changelog entries](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/37053), project visibility
- **Package Registries, what's in there for me.** Specifically C/C++ with Conan, Docker, DEB/RPM, etc. from a developer's view.

#### Kubernetes Monitoring

- https://github.com/kubernetes/kube-state-metrics
- https://www.replex.io/blog/kubernetes-in-production-the-ultimate-guide-to-monitoring-resource-metrics
- https://sysdig.com/blog/kubernetes-monitoring-prometheus/

Jaeger Tracing -> Prometheus -> Exemplars in Grafana -> combined with Loki




### Talks

#### Monitoring/Observability

- From Monitoring to Observability: Migration challenges from Blackbox to Whitebox
- From Monitoring to Observability: Make your developers wanna do traces
- From Monitoring to Observability: Migrating from Icinga to Prometheus
- From Monitoring to Observability: Monitoring challenges in a cloud native world
- GitOps without Monitoring is not Observability (title does not make sense, but that's the whole point)
- Event correlation: How to be the next on-call hero
- Failure is a good thing: How Chaos Engineering improved our on-call strategies
- Metrics and logs: Moving from number to structured strings, does it still scale?

#### DevSecOps

- From manual work to automation: Packages for everyone _(The idea of having packages for everything, and build them with CI tools. Showcase the journey from Jenkins/Hudson, build VMs, Docker introduction, fun times with SLES subscriptions, GitLab CI, attempts with GitHub actions, and the overall missing package registries (aptly's future is unclear))_
- It's a bug, not a feature: Manual work _(Prospect is the workflow of installing software, configuring it, adding it to monitoring, alerts, generate documentation, etc.)_
- Become a chaos monkey, not a DevOps engineer _(snatchy title for pushing the chaos engineering principle over the wrong jot title)_


#### Development

- Git, Containers, CI: How I became a better Open Source Developer
- As a developer, I don't care about Kubernetes


### Webcasts and Trainings

- GitLab CI for packagers. _(This is a personal passion of mine enabling open source projects with easy to install methods)_
    - Involves building knowledge on the RPM/DEB basics (pick one) and then incorporating best practices into a build container, plus publishing based on CI config (only: tag).
    - Collect ideas how to build out a deb/rpm package pipeline (with later adding another webcast once available in GitLab)
- Monitoring/Observability for CI pipelines.
    - Include daily stories like "Jobs are stuck, developer's are slacking off (xkcd ref)" with just the runner being gone.
    - Highlight the "D" in CI/CD with performance and tracing of deployed environments.
    - Use Jaeger Tracing as an advanced example to gather insights.
    - Maybe include DAG, Parent/Child Pipelines.
- Securing your CI pipeline. DevSecOps.
    - Different aspects covered, secret handling, caches, pipeline permissions, securing deployed containers and registries.
- Monitor your MySQL server with GitLab
    - Not yet fully there, but with observability moved to core, we definitely should follow along with all topics.
    - Monitor basically any service
    - Explain how to create alert rules
    - Dig into SLO/SLA reporting capabilities (early starting)

Workshops/Trainings should cover exercise slides including solutions, following my work from the NETWAYS trainings ([basic](https://github.com/netways/gitlab-training) ans [advanced](https://github.com/netways/gitlab-advanced-training)).

- GitLab 101: Basics, MR Workflows, simple CI
- GitLab for Developers: The Go example with CI builds, unit tests, coverage reports, container builds, registry, etc.

Workshops should cover the range from 2h to 4h. **Report the feedback back to the professional service trainings.**


### Demos

**Enable GitLab heroes and influencers with technical material, training classes, presentation demos.**

Implement these ideas and share them in our new project on GitLab.com.

- Make https://everyonecancontribute.com available to the team, we could e.g. use sub-domains for our efforts.
- Create a self contained website which uses the [animated tanuki](https://gitlab.com/dnsmichi/animated-tanuki). E.g. as a Docker image ready for usage, based on Nginx.
    - This could include some funny gimmicks to enable people to follow TE, e.g. a Twitter stream
    - Use a specific TE namespace in the Docker registry on GitLab.com
- Provide a Docker/Vagrant setup which fires up a complete monitoring/observability stack with Prometheus, Grafana and Exporters.
    - Should run standalone with pre-seed on configurations
    - Include metrics from everyonecancontribute.com (publicly available)
- Build a GitLab instance specifically for the observability and log management stack.
    - Play with prototypes, e.g. Elasticsearch as backend, dump logs with filebeat and try to visualize that.
    - Keep the product managers in the loop.
- Cover more distributions/containers
    - RHEL 8 with app streams are new, how does this affect developers with their repository dependencies and EPEL
    - Windows and macOS builds
    - Package/container registry
- Evaluate how IoT, MQTT and GitLab fit together, build out demos, e.g. for Raspberri Pi

### Social

Develop a strategy which pushes daily/weekly activity.

- Not only share interesting tech insights, but propose an opinion on that.
- Regularly check self-curated Twitter lists and engage with it.
- Collect a list of good tweets and share with our social team.
- Encourage team members to share tweets in #tech-evangelism Slack channel.
- Watch GitLab Unfiltered on YouTube and collect the best news
    - Release highlights to come
    - Feedback and discussions, e.g. on Release Think Big, Iteration Office Hours, etc.

### Product Feedback Loop

- [x] **Watch** Monitor & AP
    - [x] Watched the discussion: https://youtu.be/aDfUaDqrh60
    - [x] Asked for epics: https://twitter.com/dnsmichi/status/1212896402201874433
    - [x] Epics: [Add more dashboard chart types](https://gitlab.com/groups/gitlab-org/-/epics/1897) & [APM: Infrastructure Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2135) - [My feedback](https://gitlab.com/groups/gitlab-org/-/epics/1897#note_267291038)

Share ideas with the product teams:

- Dashboards
    - Keep using the feedback from Dashing for Icinga, private project: https://twitter.com/Mikeschova/status/1229415489627181056
- TLS monitoring: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34943
    - Icinga cluster with CSR auto-signing, etc.: https://icinga.com/docs/icinga2/latest/doc/19-technical-concepts/#cluster
    - Prometheus `ssl_exporter` and `blackbox_exporter` projects - not so easy to configure/deploy
    - Prometheus Exporter Security with TLS
- Product comparison: https://about.gitlab.com/devops-tools/nagios-vs-gitlab.html
    - Service and Hardware monitoring
- Cloud & container monitoring
    - Auto-discovery
    - Cloud dashboards, like Crossplane
- Implement OpenTracing/Jaeger support
    - C++ for Ceph https://github.com/opentracing/opentracing-cpp https://github.com/jaegertracing/jaeger-client-cpp
    - Go
    - Ruby
- Log aggregation and event pipelines


### Contribution areas

#### Prometheus

##### TLS

Exporters need TLS support, node_exporter PR: https://github.com/prometheus/node_exporter/pull/1277

- Add this to existing exporters in the community?
- Create a skeleton exporter which implements these changes

##### Alerts, Downtimes and Reporting

Whenever a maintenance window is set by admins, alerts should be suppressed. This can be achieved with alert manager rules.
In addition to these suppressed alerts, the downtime maintenance window should be persisted over time.

Rationale: When calculating SLA reports for the service availability, the maintenance downtime window should be removed from
possible alert conditions influencing the calculations.

Alerts themselves are already stored inside the Prometheus TSDB.

Existing addons:

- https://promtools.matthiasloibl.com/
- https://github.com/metalmatze/slo-libsonnet



### Research Areas

- Explore all GitLab features
    - By the end of 2020, have had meetings / coffee chats with all product managers and a steady feedback loop.
    - Be able to explain the product vision to the wider community in specific areas.

#### Programming Languages

- Programming languages and their new prospects.
    - Use this knowledge to share ideas with the CI Template Group as well as generate new blog content.
- Golang: Stay up-to-date and pick a project to frequently work on it and practice my programming skills.
    - CI runners
    - [Release CLI tool](https://gitlab.com/gitlab-org/gitlab-releaser)
- Ruby on Rails: Take a course and be able to contribute.
- Vue.js: Dive deeper into it, contribute to GitLab and use that knowledge for side projects and demos.

#### Tools: Monitoring/Observability

- Prometheus
    - SLO/SLA reporting
    - Clustering and high availability
- Enterprise offerings: Datadog, Honeycomb, etc.
    - Evaluate their observability stack, test APIs and integrations and gather insights for talks and technical deep dives.
        - https://www.honeycomb.io/overview/
        - https://www.honeycomb.io/serverless/
        - https://www.honeycomb.io/microservices/
- AWS Cloudwatch
- New observability/monitoring tools, evaluate their ideas.
- Follow the lead from Grafana with Loki, Prometheus and a combined correlation stack.

#### Tools: Automation & Security

- Falco [graduated](https://thenewstack.io/cncfs-falco-runtime-security-tool-graduates-from-the-sandbox-moves-into-incubation/) to CNCF incubated
    - https://falco.org/ "Falco, the open source cloud-native runtime security project, is the defacto Kubernetes threat detection engine. Falco detects unexpected application behavior and alerts on threats at runtime."
    - Try it: https://falco.org/docs/installation/
- Testing your RedTeam Infrastructure https://blog.xpnsec.com/testing-redteam-infra/ (CI, inSpec, Molecule)
- Keep an eye on the config management community.
    - Strategy of Puppet, Ansible, Salt, Chef and new to the game, mgmt.
- Security
    - Hashicorp Vault alternatives
    - CI Secret management
    - Follow Patrick Debois from SnykSec and his ideas, https://www.youtube.com/watch?v=pX7VPVcEfMM
- Kubernetes, Docker, Podman, Openshift, etc. seen as evolving area with different directions.
- Repository management with Pulp: https://pulpproject.org/2019/12/12/pulp-3-is-GA/
- Puppet's Lyra project, cloud native workflows, got canceled. Still, the tools are available: https://github.com/lyraproj/hiera (Hiera written in Go)
