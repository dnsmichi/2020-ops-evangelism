# Michael 2020 Developer Evangelism Plan

1. [Purpose](#purpose)
1. [Focus Area](#focus-area)
    - 2.1. [Audience](#audience)
    - 2.2. [Team Focus](#team-focus)
    - 2.3. [My Focus](#my-focus)
    - 2.4. [Feedback Loop](#feedback-loop)
    - 2.5. [Metrics](#metrics)
1. [Ideas and Reasearch](#ideas-and-research)


## Purpose

This document provides insights into my Technical Evangelism plans for 2020. It covers my story telling and thought leadership evolvement as Developer Evangelist on our team.

Not everything written here is going to happen, and thoughts and ideas may change over time. When linking or referring to here, please keep this in mind
and rather ask via Slack or schedule a short meeting to clear things up. In case you want to follow along with specific topics and need our TE resources, please follow our [request template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/technical-evangelist-request.md) and create a new issue in the [Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues).

## Focus Area

### Audience

- Open source and online communities
- Social media followers and hashtags: #devops, #devsecops, #observability, #monitoringlove, #cfgmgmtcamp
- SIG members (CNCF, CDF)
- Competitors, pushing towards discussion and thought exchange
- Business leaders seeing our actions

### Team Focus

- Combine the Dev, Sec, Ops and Cloud native areas and become experts all together.
- Foster a great relationship with Open source communities.
- Take the lead and become a role model for users, heroes and community evangelist.
- Enable the GitLab team to efficiently access our resources and knowledge.
- Share our background and experiences in our team and push each other. Everyone has unique items in their chest.
- Support Brendan in CI/CD with developer specific variations (C++, etc.)
- Be open for collaborations and chats
- TE Collaboration or Think Big sessions in Zoom/YT streams

### My Focus

> I will become a known educator in observability and CI/CD, deciphering the work and points of view of thought leaders in the space for the wider developer and operations community.

Become a community tech leader with helping users. People's trust is earned with helping them and later engage. Follow Kelsey Hightower's path.

- [Prometheus](https://prometheus.io/community/)
- [Grafana](https://community.grafana.com/)
- [GitLab](https://forum.gitlab.com)
- [Elastic](https://discuss.elastic.co/), [Graylog](https://community.graylog.org/)

In addition to my focus areas, I will be active in the German speaking countries and building localized content for DACH.
This helps with building my profile, and will not be my primary focus.


#### Observability/Monitoring

With my background of leading an open source monitoring project for ~11 years in both, backend development and community support, I have a strong knowledge about challenges and solutions the monitoring world. With the shifted mindset moving from classic service monitoring to a cloud native observability world, we need to raise awareness and propose best practices. A container cluster in Kubernetes is able to heal itself, and poll-based monitoring tools cannot determine its state reliably. 

- Join Prometheus and Grafana communities (if not done already)
- Build out dashboards for observability. Grafana is becoming a monolith, where is the competition?
- Focus on community and enterprise needs: Easy-to-start, incident management and SLO/SLA reporting.
- Learn from GitLab ops team and propose best practices together
    - Create Grafana dashboards, push towards community feedback, etc. - make it easier for admins.

#### CI/CD

I'm coming from the administration and ops side with having managed large infrastructures in student dorms, at the University of Vienna/ACO.net and at NETWAYS. The pain without automation (Puppet, Ansible) and doing manual work is something I praise to avoid. I'm active in the Dev(Sec)Ops community with proposing workflows, learned lessons and anticipate in culture discussions at [DevOpsDays](https://www.netways.de/en/blog/2019/10/31/devopsdays-ghent-celebrating-10-years-devops-culture/). Additional venues came from this community, with Config Management Camp and DeliveryConf where I can push my thought leadership.

CI/CD matches both, Dev and Ops. With my background of backend development (C/C++, Golang) I'm able to understand typical developer workflows and problems, shifting the mindset to operations where the software is actually run and battle-tested. Release management covers the project management part with the last pipeline in deploying to production. Our mission is to enable people to automate this workflow and do not halt before the production job is run.

- Join CDF projects with contributions (Tekton, Spinnaker) and improve their integrations with GitLab
- Join Ansible/Puppet community projects and help with contributions (code, docs, community)
- Make the deployment automation process easier for everyone with thoughtful content and help


##### Packages for everyone

Collect feedback on what open source developers need, the easy-to-install experience with packages. Keep best practices for different language, as C++ requires compiling whereas Node.js and Ruby provoke a long list of dependency management.

Build everything together and aim to integrate/replace Artifactory in the long term.

- Relevant for learning and pushing the product: https://about.gitlab.com/direction/package/package_registry/
- Example guide from GitLab: https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/index.html

##### Supercharge the Admin

- Enable them with self-monitoring and health status
- Allow them to understand which CI components their developers need.
- Enable them to use Docker and build images, and improve the process (e.g. matrix builds)


#### Contributions

Develop thought leadership with:

- Contribute to the Prometheus project and join Ben Kochie
    - Help maintain node exporters, add packages (RPM/DEB), help with build tooling, documentation
    - Explore ways of SLO/SLA reporting for the enterprise needs
    - Community help and social
    - Working issue: https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/2014
- Contribute to Grafana and its ecosystem (Loki, Tanka, etc.)
- Contribute to Tekton, Spinnaker (CDF)
- Contribute to Puppet/Ansible/etc. _(minimal effort based on current experience and/or research)_
- Share webcasts, demos and snippets/code which help people solve their problem or get a free training.

#### Community 

- Conferences and events in the Ops area: Monitoring/Observability, config management, CI/CD
- SIGs, e.g. [Security](https://github.com/cdfoundation/sig-security)
- Live-tweet from events with specific hash tags, push strong opinions in tweets, not just slide summaries.
- Open source community forums, developer mailing lists and channels
- GitHub issues (and discussions when implemented (beta))
- YouTube/Twitch webcasts/podcasts
- Newsletters (quality content and tweets)
- Meetups and local conferences (from smaller to bigger, sharing a message is sometimes easier in smaller groups)
- Local content in German, published media articles and blog posts


#### Content

**Enable GitLab heroes and influencers with technical material, training classes, presentation demos.**

Develop a strategy which pushes daily/weekly activity:

- Not only share interesting tech insights, but propose an opinion on that.
- Regularly check self-curated Twitter lists and engage with it.
- Collect a list of good tweets and share with our social team.
- Encourage team members to share tweets in #tech-evangelism Slack channel.
- Watch GitLab Unfiltered on YouTube and collect the best news
    - Release highlights to come
    - Feedback and discussions, e.g. on Release Think Big, Iteration Office Hours, etc.

##### Platforms

Use the following platforms:

- Conferences and events in the Ops area
- Live-tweet from events with specific hash tags, push strong opinions in tweets, not just slide summaries.
- Open source community forums
- Developer mailing lists and channels
- GitHub issues (and discussions when implemented (beta))
- YouTube/Twitch webcasts/podcasts
- Newsletters (quality content and tweets)
- Meetups and local conferences (from smaller to bigger, sharing a message is sometimes easier in smaller groups)
- Local content in German, published media articles and blog posts    

External platforms:

- [My blog](https://dnsmichi.at) (generate reach via Twitter/LinkedIn shares)
- [Dev.to](https://dev.to/dnsmichi)
- [Medium](https://medium.com/@dnsmichi)
- [Opensource.com](https://opensource.com)
- Newsletters: [Monitoring.weekly](https://monitoring.love/), [cron.weekly](https://ma.ttias.be/cronweekly/), [devopsish](https://devopsish.com/categories/weekly/)
- [TheNewStack](https://thenewstack.io/) via partnership.
- [PuppetForge](https://forge.puppet.com/). Reach out to [David Schmitt](https://twitter.com/dev_el_ops).
- [Voxpupuli](https://voxpupuli.org/) being the voice of the Puppet community. Reach out to [Tim Meusel](https://twitter.com/bastelsblog).
- [InfluxDB](https://www.influxdata.com/). Reach out to [David Mc Kay](https://twitter.com/rawkode).
- [Sensu](https://sensu.io/). Reach out to [Sean Porter](https://twitter.com/portertech) and [Caleb Hailey](https://twitter.com/calebhailey).
- [Graylog](https://www.graylog.org/). Reach out to [Lennart Koopmann](https://twitter.com/_lennart).
- [Elastic](https://www.elastic.co/). Reach out to [Philipp Krenn](https://twitter.com/xeraa).

##### Blog posts

- **Prometheus for Beginners** Share my first steps with Prometheus with advancing to website monitoring, services, etc.
- **Prometheus in a distributed view** Advanced techniques I need to learn, Thanos and more.
- **From Monitoring to Observability**. Pick specific monitoring tasks and workflows, compare classic service monitoring with cloud native metric focused challenges. Add best practices e.g. on how to monitor an application cluster.
- **Build packages for better installation experience**. RPM/DEB packaging is hard to learn, a blog series and repositories will help. This will create unique content and help shape our vision with GitLab package registries.
- **Puppet**. Share insights on Puppet modules relevant to us. Work with David Schmitt on the PuppetForge experience with howtos and getting started blogs.
- **Ansible in the wild**. Galaxy is hard to use, encourage people for good playbooks. Combine this with Terraform partnerships.
- **DNS**. Include old and new things, go along with "Everything is a DNS problem". DoH (DNS over HTTPS is a sensitive topic for example)
- **Life of a Developer Evangelist** [Share](https://twitter.com/dnsmichi/status/1235210840434978818) what it is like and what we do on a daily/weekly basis to encourage others to do the same. This includes **everything** from my tech-evangelism docs, but leveraged into smaller pieces as with creating this document :)
- **Here is a trap, thank you** Tackle the problem with SNMP traps and event processing pipelines. Follow along with telemetry streaming for network hardware.
- **Incident management based on events/alerts** A common use case for monitoring integration is with ticket systems. `Alert->Ticket, Acknowledge the problem, Recovery->Close Ticket` workflows. Tackle these challenges with current tools and compare best practices. Build a knowledge base for future talks.
- **IoT Event processing with GitLab's observability stack.** Working title. Should cover the idea of IoT and smarthome devices which send e.g. their temperature. Why not find a use case how GitLab cna process that data, create fancy dashboards and update tickets or commit measurements. I wrote an [iX article](https://www.heise.de/select/ix/2018/4/1522451271629282) on IoT monitoring in the past.
- **My website is so slow. Have you heard about Tracing?**
    - Also, look into CNCF projects and verify whether they need support for OpenTracing/Jaeger/OpenTelemtry. Create PRs, and blog about these additions.
    - Example for [C++](https://www.netways.de/blog/2020/01/09/from-monitoring-to-observability-distributed-tracing-with-jaeger/)

##### Web casts and Trainings

- GitLab CI for packagers.
- Monitoring/Observability for CI pipelines.
- Securing your CI pipeline. DevSecOps.
- Monitor your MySQL server with GitLab (not yet there, we need a vision!)
- GitLab 101: Basics, MR Workflows, simple CI
- GitLab for Heroes: The Go example with CI builds, unit tests, coverage reports, container builds, registry, etc.

##### Talks

- From Monitoring to Observability: Migration challenges from Blackbox to Whitebox
- From Monitoring to Observability: Make your developers wanna do traces
- From Monitoring to Observability: Migrating from Icinga to Prometheus
- From Monitoring to Observability: Monitoring challenges in a cloud native world
- Event correlation: How to be the next on-call hero
- Failure is a good thing: How Chaos Engineering improved our on-call strategies
- Metrics and logs: Moving from number to structured strings, does it still scale?
- It's a bug, not a feature: Manual work _(Prospect is the workflow of installing software, configuring it, adding it to monitoring, alerts, generate documentation, etc.)_
- From manual work to automation: Packages for everyone


##### Demos 

- Make https://everyonecancontribute.com available to the team, we could e.g. use sub-domains for our efforts.
- Create a self contained website which uses the [animated tanuki](https://gitlab.com/dnsmichi/animated-tanuki). E.g. as a Docker image ready for usage, based on Nginx.
- Provide a Docker/k8s setup which fires up a complete monitoring/observability stack with Prometheus, Grafana and Exporters.
- Build a GitLab instance specifically for the observability and log management stack.
- Cover more distributions/containers


#### Events

Check our [TE event sheet](https://docs.google.com/spreadsheets/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit?ts=5e61878d#gid=0) for global events we will/can attend.
The list below covers local events in my region where I can reach out to.

##### Conferences

- [CfgMgmtCamp](https://cfgmgmtcamp.eu/)
- [PromCon EU](https://promcon.io)
- DevOpsDays in Europe

Cfp only:

- [ContainerDays EU](https://www.containerdays.io/)
- [Open Source Automation Days](https://osad-munich.org/)
- [Continuous Lifecycle](https://www.continuouslifecycle.de/) presented by Heise/iX. 
- [ContainerConf](https://www.containerconf.de/) presented by Heise/iX.
- [OSMC](https://osmc.de). 


##### Meetups

Meetups are events that I plan to attend on a regular basis, if traveling makes sense. Everything which is located in the Nuremberg/Munich area can be reached within 2 hours range. Berlin/Vienna is 4h+ away, would need accommodation. Friends are located in the bigger cities in Austria/Germany.

- [DevOps Camp Nuremberg](https://devops-camp.de/). Estimated audience reach: 200.
- [Kubernetes Nuremberg (CNCF Group)](https://www.meetup.com/Kubernetes-Nurnberg/)
- [DevOps Nuremberg](https://www.meetup.com/devops-nuernberg/)
- [Hackerkegeln by DATEV](https://hackerkegeln.de/)
- [GitLab Meetup Berlin](https://www.meetup.com/berlin-gitlab-meetup/)
- [Prometheus Berlin](https://www.meetup.com/Berlin-Prometheus-Meetup/)
- [GitLab Meetup Hamburg](https://www.meetup.com/GitLab-Meetup-Hamburg/)
- [Icinga Users Berlin](https://www.meetup.com/Icinga-Users/)
- [DevOps & Security Vienna](https://www.meetup.com/Vienna-DevOps-Security/)
- [Icinga Users Austria](https://www.meetup.com/Icinga-Users-Austria/)
- [GitLab Meetup London](https://www.meetup.com/London-Gitlab-Meetup-Group/)
- [Linuxing in London](https://www.meetup.com/Linuxing-In-London/)
- [Metrics and Monitoring London](https://www.meetup.com/London-Metrics-and-Monitoring/)


### Feedback Loop

Create a feedback loop with product managers:

- Ops+CI, Monitor: Kevin Chu
- Ops+CI, Health: Sarah Waldner
- Ops+CI, Package: Tim Rizzi
- Ops+CI, Release: Jackie Meshell
- Ops+CI, Templates: Thao Yeager

#### Product Feedback

Share ideas with the product teams:

- Dashboards
    - [Discussion](https://youtu.be/aDfUaDqrh60)
    - Epics: [Add more dashboard chart types](https://gitlab.com/groups/gitlab-org/-/epics/1897) & [APM: Infrastructure Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2135) - [My feedback](https://gitlab.com/groups/gitlab-org/-/epics/1897#note_267291038)
- TLS monitoring: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34943
    - Prometheus `ssl_exporter` and `blackbox_exporter` projects - not so easy to configure/deploy
    - Prometheus Exporter Security with TLS
- Product comparison: https://about.gitlab.com/devops-tools/nagios-vs-gitlab.html
    - Service and Hardware monitoring
- Cloud & container monitoring
    - Auto-discovery
    - Cloud dashboards, like Crossplane
- Implement OpenTracing/Jaeger support
    - [C++](https://github.com/opentracing/opentracing-cpp https://github.com/jaegertracing/jaeger-client-cpp), Go, Ruby
- Log aggregation and event pipelines (ES, Loki)

#### Dogfooding

We also will be using GitLab.com differently to others.

- TE Organization, Issues, Epics, Issue Boards.
- Content and code snippets, re-usable libraries and clients for production.
- Demo playgrounds
- Manage TODO lists
- Extract metrics from our wide-spread activities

From these experiences, evaluate whether others could benefit from this as well. Propose new product ideas to the team. One item are for example replacing Microsoft To-Do with a GitLab app (discussed that with Brendan on Twitter a while ago).

- Allow to add notes, labels, organize them. Replace Microsoft To-Do/Wunderlist, Evernote, etc. with reminders also showing up
- Maybe explore a whole new feature area, outside of GitLab issues and project management


### Metrics

In order to measure our progress, we need to record and track our activities. Any request being made must follow the TE request template. If we create our own issues, the TE labels must be applied for tracking it in our [issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?label_name[]=tech-evangelism). 

In addition to following our own progress, I will be channeling feedback to the product managers and everyone else at GitLab. 

Tentative metrics idea per month:

- 2+ blog posts per month on external platforms
- 1 blog post on GitLab.com
- 1+ MR at GitLab, Runners, Docs or where applicable
- Review or help review 1 MR 
- 5+ MRs to open source projects in the ops area. If not fulfilled, track issue discussions and reviews.

General metrics collection (TBD):

- Participation in TE requested help.
    - Raised demand for TE support, measured from request rate.
    - Blog posts published, with the main focus on quality not quantity.
- Twitter analytics. Tweet audience reach, interactions, likes, retweets. Toy and test things with different phrasing and topics, report these metrics.
- External guest blog posts
- Visibility in encouraging and pushing users
- Audience reach and activity in community channels (e.g. impact on GitLab Discourse)
- Highlights in newsletters
- Back reference links to workshops and trainings
- Mentions/participation in product feedback issues (needs organization/tracking, may be automated with actions)
    - Kindly ask product managers to add their feedback
    - Consider having a release retrospective item with how much TE input they had


Personal metrics:

- By the end of 2020, have had meetings / coffee chats with all product managers and a steady feedback loop.
- Be able to explain the product vision to the wider community in specific areas.


## Ideas and Research

### Programming Languages

- Programming languages and their new prospects.
    - Use this knowledge to share ideas with the CI Template Group as well as generate new blog content.
- Golang: Stay up-to-date and pick a project to frequently work on it and practice my programming skills.
    - CI runners
    - [Release CLI tool](https://gitlab.com/gitlab-org/gitlab-releaser)
- Ruby on Rails: Take a course and be able to contribute.
- Vue.js: Dive deeper into it, contribute to GitLab and use that knowledge for side projects and demos.

### Tools: Monitoring/Observability

- Prometheus
    - SLO/SLA reporting
    - Clustering and high availability
- Enterprise offerings: Datadog, Honeycomb, etc.
    - Evaluate their observability stack, test APIs and integrations and gather insights for talks and technical deep dives.
- AWS Cloudwatch
- New observability/monitoring tools, evaluate their ideas.
- Follow the lead from Grafana with Loki, Prometheus and a combined correlation stack.





